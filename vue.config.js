module.exports = {
  devServer: {
    proxy: {
      "/getCS": {
        "target": "http://localhost:3000",
        "secure": false,
        "logLevel": "debug",
        "pathRewrite": {
          "^/getCS": ""
        },
        "changeOrigin": true
      },
    }
  }
}