import Vue from "vue";
import Vuetify from 'vuetify';
import './plugins/vuetify';

import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/icon-material.css';
import './assets/css/custom.css';

import 'popper.js/dist/popper.min.js'
import 'bootstrap/dist/js/bootstrap.min.js';


Vue.use(Vuetify);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
